import Vue from 'vue'
import VueRouter from 'vue-router'
import FirstTask from '../views/FirstTask.vue'
import SecondTask from '../views/SecondTask.vue'

Vue.use(VueRouter)

const routes = [{
    path: '/',
    name: 'FirstTask',
    component: FirstTask
  },
  {
    path: '/goods',
    name: 'SecondTask',
    component: SecondTask,
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router