import axios from "axios";

export default axios.create({
  baseURL: "https://vue-study.dev.creonit.ru/api",
});
